

package dataviewer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

public class DataViewer {

 
    public static void main(String[] args) {
        
        Connection c = null;
        int liczbaKolumn =0;
      try {
         Class.forName("org.postgresql.Driver");
         c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/test","postgres", "post");
         Statement st = c.createStatement();
         ResultSet rs = st.executeQuery("select * from serie");
         ResultSetMetaData rsmd = rs.getMetaData();
         liczbaKolumn = rsmd.getColumnCount();
         
         while(rs.next())
         {
            for(int i=1;i<=liczbaKolumn;i++)
            {
                if(i>1)
                    System.out.print(" | ");
                System.out.print(rs.getString(i));
            }
            System.out.println("");
         }
         
      } catch (Exception e) {
         e.printStackTrace();
         System.err.println(e.getClass().getName()+": "+e.getMessage());
         System.exit(0);
      }
        
      
      
        
        
    }
    
}
